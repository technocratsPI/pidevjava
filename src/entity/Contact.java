/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author hajri
 */
public class Contact {
    
    private Integer id;
    
    private String suijet;
    
    private String mail;
    
    private String descrption;
    
    private String etat;
    
    private String image;

    public Contact() {
    }

    public Contact(Integer id, String suijet, String mail, String descrption, String etat, String image) {
        this.id = id;
        this.suijet = suijet;
        this.mail = mail;
        this.descrption = descrption;
        this.etat = etat;
        this.image = image;
    }

    public Contact(String suijet, String mail, String descrption, String etat, String image) {
        this.suijet = suijet;
        this.mail = mail;
        this.descrption = descrption;
        this.etat = etat;
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSuijet() {
        return suijet;
    }

    public void setSuijet(String suijet) {
        this.suijet = suijet;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getDescrption() {
        return descrption;
    }

    public void setDescrption(String descrption) {
        this.descrption = descrption;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    
    

    
}
