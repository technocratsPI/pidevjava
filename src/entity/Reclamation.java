/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author hajri
 */
public class Reclamation {
    private Integer id;
 
    private String suijet;
    
    private String mail;
    
    private String descrption;
    
     
    private int idUEnv;
    
    private int idUCr;

    public Reclamation() {
    }

    public Reclamation(String suijet, String mail, String descrption, int idUEnv, int idUCr) {
        this.suijet = suijet;
        this.mail = mail;
        this.descrption = descrption;
        this.idUEnv = idUEnv;
        this.idUCr = idUCr;
    }




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSuijet() {
        return suijet;
    }

    public void setSuijet(String suijet) {
        this.suijet = suijet;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getDescrption() {
        return descrption;
    }

    public void setDescrption(String descrption) {
        this.descrption = descrption;
    }

    public int getIdUEnv() {
        return idUEnv;
    }

    public void setIdUEnv(int idUEnv) {
        this.idUEnv = idUEnv;
    }

    public int getIdUCr() {
        return idUCr;
    }

    public void setIdUCr(int idUCr) {
        this.idUCr = idUCr;
    }

 
    
    
}
