/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Objects;

/**
 *
 * @author hajri
 */
public class user {
    private Integer id;
    
    private String nom;
    
    private String prenom;
    
    private String photo;
    
    private String username;
    
    private String  email;
    
    private String password;
    
    private int role;
    
    private String statut;

    private int nb_reclamation;

    public user(Integer id, int nb_reclamation) {
        this.id = id;
        this.nb_reclamation = nb_reclamation;
    }

    public int getNb_reclamation() {
        return nb_reclamation;
    }

    public void setNb_reclamation(int nb_reclamation) {
        this.nb_reclamation = nb_reclamation;
    }

    
            

    public user() {
    }

    public user(Integer id, String nom, String prenom, String username, String email, String statut) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.email = email;
        this.statut = statut;
    }
    

    public user(Integer id, String nom, String prenom, String photo, String username, String email, String password, int role, String statut) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.photo = photo;
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
        this.statut = statut;
    }

    public user(String nom, String prenom, String username, String email, String password, int role, String statut) {
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
        this.statut = statut;
    }

    public user(String nom, String prenom, String photo, String username, String email, String password, int role, String statut) {
        this.nom = nom;
        this.prenom = prenom;
        this.photo = photo;
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
        this.statut = statut;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    @Override
    public String toString() {
        return "user{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", photo=" + photo + ", username=" + username + ", email=" + email + ", password=" + password + ", role=" + role + ", statut=" + statut + '}';
    }
     
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof user)) {
            return false;
        }
        user other = (user) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }    
   
}
