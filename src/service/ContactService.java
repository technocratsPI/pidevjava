/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import connexionBd.ConnexionBD;
import entity.Contact;
import entity.user;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author hajri
 */
public class ContactService implements Iservice<Contact> {

    private Connection connection;
    private Statement ste;
    private PreparedStatement pst ;
    private ResultSet res ;

    public ContactService() {
        connection = ConnexionBD.getInstance().getCnx();
    }


    @Override
    public void insert(Contact f) {
        String requete ="insert into contact (suijet,mail,descrption,etat,image) value('"+f.getSuijet()+"','"+f.getMail()+"','"+f.getDescrption()+"','"+f.getEtat()+"','"+f.getImage()+"')"; 
        try {
            ste=connection.createStatement();
            ste.executeUpdate(requete);
            
        } catch (SQLException ex) {
            Logger.getLogger(ContactService.class.getName()).log(Level.SEVERE, null, ex);
        }    }


    @Override
    public void update(Contact f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Contact> getAll() {
          ObservableList<Contact> list = FXCollections.observableArrayList();
        String requete = " select * from contact" ;
        try {
            ste = connection.createStatement();
            res=ste.executeQuery(requete);
            while(res.next())
            {  
                list.add(new Contact(res.getInt(1),res.getString(2),res.getString(3),res.getString(4),res.getString(5),res.getString(6)));
      
            }
            }
        catch (SQLException ex) {
            Logger.getLogger(user.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;     
    }

    @Override
    public Contact getById(Contact f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Contact searchByMail(Contact f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Contact searchByUsrname(Contact f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Contact f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
