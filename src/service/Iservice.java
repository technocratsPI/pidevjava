/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import javafx.collections.ObservableList;

/**
 *
 * @author hajri
 */
public interface Iservice <T> {
   void insert(T f);
   void delete(T f);
   void update(T f);
    
    ObservableList<T>getAll();
    T getById(T f);
    T searchByMail(T f);
    T searchByUsrname(T f);

}
