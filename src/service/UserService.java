/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import connexionBd.ConnexionBD;
import entity.user;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author hajri
 */
public class UserService implements Iservice<user>{
    
    private Connection connection;
    private Statement ste;
    private PreparedStatement pst ;
    private ResultSet res ;
    
    public UserService() {
        connection=ConnexionBD.getInstance().getCnx();
    }
   @Override
    public void insert(user f) {
        String requete ="insert into test(nom,prenom,username,email,password,role,statut) value('"+f.getNom()+"','"+f.getPrenom()+"','"+f.getUsername()+"','"+f.getEmail()+"','"+f.getPassword()+"','"+f.getRole()+"','"+f.getStatut()+"')";                              
        String requete2 ="insert into test(nom,prenom,photo,username,email,password,role,statut) value('"+f.getNom()+"','"+f.getPrenom()+"','"+f.getPhoto()+"','"+f.getUsername()+"','"+f.getEmail()+"','"+f.getPassword()+"','"+f.getRole()+"','"+f.getStatut()+"')";                              

        try {
            ste=connection.createStatement();
            if( f.getPhoto() == null )
            ste.executeUpdate(requete);
            else{
            ste.executeUpdate(requete2);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }

    
    @Override
    public user searchByMail(user f) {
 String requete = " select * from test where email='"+f.getEmail()+"'";
         
         try {
           
            ste = connection.createStatement();
            res=ste.executeQuery(requete);
            if (res.next())
            {   
                f = new user(res.getInt(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getInt(8),res.getString(9));
            }
            else
                f=null;
           // System.out.println(t+"search by mail");
        } catch (SQLException ex) {
            Logger.getLogger(user.class.getName()).log(Level.SEVERE, null, ex);
        }
        return f ;    
    }

    @Override
    public user searchByUsrname(user f) {
 String requete = " select * from test where username='"+f.getUsername()+"'";
         
         try {
           
            ste = connection.createStatement();
            res=ste.executeQuery(requete);
            if (res.next())
            {   
                f = new user(res.getInt(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getInt(8),res.getString(9));
            }
            else
                f=null;
           // System.out.println(t+"search by mail");
        } catch (SQLException ex) {
            Logger.getLogger(user.class.getName()).log(Level.SEVERE, null, ex);
        }
        return f ;        
    }

   

    @Override
    public void update(user f) {
try {
            String requete = " update test set nom='"+f.getNom()+"' , prenom='"+f.getPrenom()+"' , photo='"+f.getPhoto()+"' , username='"+f.getUsername()+"' , email='"+f.getEmail()+"'  , password='"+f.getPassword()+"',role='"+f.getRole()+"',statut='"+f.getStatut()+"' where id='"+f.getId()+"'" ;
            ste=connection.createStatement();
            ste.executeUpdate(requete);
            
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }

    @Override
    public ObservableList<user> getAll() {
          ObservableList<user> list = FXCollections.observableArrayList();
        String requete = " select * from test" ;
        try {
            ste = connection.createStatement();
            res=ste.executeQuery(requete);
            while(res.next())
            { 
                list.add(new user(res.getInt(1), res.getString(2),res.getString(3), res.getString(4), res.getString(5), res.getString(6),res.getString(7),res.getInt(8),res.getString(9) )) ;
            }
            }
        catch (SQLException ex) {
            Logger.getLogger(user.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list ;    }

    @Override
    public user getById(user f) {
        String requete = " select* from test where id='"+f.getId()+"'";
        try {
           
            ste = connection.createStatement();
            res=ste.executeQuery(requete);
            if (res.next())
            {
                f = new user(res.getInt(1), res.getString(2),res.getString(3), res.getString(4), res.getString(5), res.getString(6),res.getString(7),res.getInt(8),res.getString(9));
          }
        } catch (SQLException ex) {
            Logger.getLogger(user.class.getName()).log(Level.SEVERE, null, ex);
        }
        return f ;    
    }
 public int conteur(user f) {
            int a = 0;
        String requete = " select nb_reclamation from test where id='"+f.getId()+"'";
        try {
           
            ste = connection.createStatement();
            res=ste.executeQuery(requete);
               a = res.getInt(1)+1;
  
               
               
        } catch (SQLException ex) {
            Logger.getLogger(user.class.getName()).log(Level.SEVERE, null, ex);
        }
        return a ;    
    }
      
    public void updateconteur(user f) {
try {
            String requete = " update test set  nb_reclamation='"+f.getNb_reclamation()+"' where id='"+f.getId()+"'" ;
            ste=connection.createStatement();
            ste.executeUpdate(requete);
            
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
    public void blocker(user f) {
try {
              f.setStatut("susspendu");
            String requete = " update test set  statut='"+f.getStatut()+"' where id='"+f.getId()+"'" ;
            ste=connection.createStatement();
            ste.executeUpdate(requete);
            
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }

    @Override
    public void delete(user f) {
          try {
            String requete = " delete from test where id='"+f.getId()+"'" ;
            ste=connection.createStatement();
           ste.executeUpdate(requete);
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
    
     public ObservableList<user> getUsers() {
       ObservableList<user> list = FXCollections.observableArrayList();
        String requete = " select * from test where role=1" ;
        try {
            ste = connection.createStatement();
            res=ste.executeQuery(requete);
            while(res.next())
            {
             
                    list.add(new user(res.getInt(1), res.getString(2),res.getString(3), res.getString(4), res.getString(5), res.getString(6),res.getString(7),res.getInt(8),res.getString(9)));
            }
            }
        catch (SQLException ex) {
            Logger.getLogger(user.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list ;
    }
    
    
}
