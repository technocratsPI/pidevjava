/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author hajri
 */
public class AdminController implements Initializable {

    @FXML
    private AnchorPane anchoradmin;
    @FXML
    private Tab reclamationbtn;
    @FXML
    private Tab contactbtn;
    @FXML
    private Tab deconection;
    @FXML
    private Tab userbtn1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void gotouser(Event event) throws IOException {
       AnchorPane pane = FXMLLoader.load(getClass().getResource("affichageUser.fxml"));
           anchoradmin.getChildren().setAll(pane);
    }

    @FXML
    private void gotoreclamation(Event event) {

    }

    @FXML
    private void gotocontact(Event event) throws IOException {
               AnchorPane pane = FXMLLoader.load(getClass().getResource("ContactAffichage.fxml"));
           anchoradmin.getChildren().setAll(pane);

    }

    @FXML
    private void gotoss(Event event) throws IOException {
               AnchorPane pane = FXMLLoader.load(getClass().getResource("ss.fxml"));
           anchoradmin.getChildren().setAll(pane);
    }
    
}
