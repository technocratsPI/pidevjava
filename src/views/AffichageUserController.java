/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import entity.Service;
import entity.user;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import service.UserService;

/**
 * FXML Controller class
 *
 * @author hajri
 */
public class AffichageUserController implements Initializable {

    @FXML
    private AnchorPane anchorUser;
    @FXML
    private TableView<user> tab_user;
    
    ObservableList<user> data =FXCollections.observableArrayList();
    FilteredList<user> filteredData ;
    
    @FXML
    private JFXTextField searchfield;
    @FXML
    private ImageView photoprofile;
    @FXML
    private Label Uname;
    @FXML
    private Label UserEmail;
    @FXML
    private Label UserStatut;
    private JFXButton blockerbtn;
    @FXML
    private Button back;
    @FXML
    private JFXButton recbtn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        //tableuser initailiseur
        
        TableColumn<user,Integer> columnUserid =new TableColumn("Id");
        TableColumn<user,String> columnUserNom =new TableColumn("Nom");
        TableColumn<user,String> columnUserPrenom =new TableColumn("Prenom");
        TableColumn<user,String> columnUserMail =new TableColumn("Email");
        TableColumn<user,String> columnUserUsername =new TableColumn("Username");
        TableColumn<user,String> columnUserState =new TableColumn("Statut");
        
        
        columnUserid.setCellValueFactory(new PropertyValueFactory<>("Id"));
        columnUserNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        columnUserPrenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        columnUserMail.setCellValueFactory(new PropertyValueFactory<>("Email"));
        columnUserUsername.setCellValueFactory(new PropertyValueFactory<>("Username"));
        columnUserState.setCellValueFactory(new PropertyValueFactory<>("statut"));
        
        columnUserid.prefWidthProperty().bind(tab_user.widthProperty().divide(5));
        columnUserNom.prefWidthProperty().bind(tab_user.widthProperty().divide(5));
        columnUserPrenom.prefWidthProperty().bind(tab_user.widthProperty().divide(5));
        columnUserMail.prefWidthProperty().bind(tab_user.widthProperty().divide(5));
        columnUserUsername.prefWidthProperty().bind(tab_user.widthProperty().divide(5));
        columnUserState.prefWidthProperty().bind(tab_user.widthProperty().divide(5));
        
        ////bindcolumn
        tab_user.getColumns().addAll(columnUserid,columnUserNom,columnUserPrenom,columnUserUsername,columnUserMail,columnUserState);
        UserService serv = new UserService();
        data=serv.getUsers();
        tab_user.setItems(data);
        filteredData= new FilteredList<>(data, e -> true);
    }

    @FXML
    private void advancedSearch(KeyEvent event) {
        
        searchfield.textProperty().addListener((observableValue,oldValue,newValue) ->{
        filteredData.setPredicate((Predicate<? super user>) (user user)->{
        if(newValue == null || newValue.isEmpty())
        {return true ;}
        String lowerCaseFilter =newValue.toLowerCase();
        
        if(user.getNom().contains(newValue))
        {return true;}
        else if(user.getPrenom().toLowerCase().contains(lowerCaseFilter))
        {return true;}
        else if(user.getEmail().toLowerCase().contains(lowerCaseFilter))
        {return true;}
        else if(user.getUsername().toLowerCase().contains(lowerCaseFilter))
        {return true;}
        return false ;
    });
    });
         SortedList<user> sortedData = new SortedList<>(filteredData);
         sortedData.comparatorProperty().bind(tab_user.comparatorProperty());
         tab_user.setItems(sortedData);
        
    }

    @FXML
    private void showProfile(MouseEvent event) {
                 user user  = tab_user.getSelectionModel().getSelectedItem();
         Image img = new Image("file:"+user.getPhoto());
         photoprofile.setImage(img);
         Uname.setText(user.getUsername());
         UserEmail.setText(user.getEmail());
         UserStatut.setText(user.getStatut()+"");
        Uname.setVisible(true);
        UserEmail.setVisible(true);
         UserStatut.setVisible(true);
        
    }


    @FXML
    private void gotoadmin(ActionEvent event) throws IOException {
                       AnchorPane pane = FXMLLoader.load(getClass().getResource("admin.fxml"));
           anchorUser.getChildren().setAll(pane);
    }

    @FXML
    private void gotoreclmation(ActionEvent event) throws IOException {
             
          user user  = tab_user.getSelectionModel().getSelectedItem();
              
          FXMLLoader loder =new FXMLLoader();
                        loder.setLocation(getClass().getResource("Reclamtion.fxml"));
                        loder.load();
                        
                        ReclamtionController display=loder.getController();
                        
                        display.setUser(user);
                        Parent ana=loder.getRoot();
                        
                        Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
                        
                        Scene ajoutScene=new Scene(ana);
                        window.setScene(ajoutScene);
                        window.show();

    }
    
    
}
