/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import entity.Contact;
import entity.user;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import service.ContactService;
import service.UserService;

/**
 * FXML Controller class
 *
 * @author hajri
 */
public class ContactAffichageController implements Initializable {

    @FXML
    private TableView<Contact> tab_contact;
    @FXML
    private JFXTextField serachfield;
    @FXML
    private JFXTextArea descrption;
    @FXML
    private Label Suijet;
    @FXML
    private Label email;
    @FXML
    private ImageView photo;
    
    ObservableList<Contact> data =FXCollections.observableArrayList();
    FilteredList<Contact> filteredData ;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         TableColumn<Contact,Integer> columnContactid =new TableColumn("Id");
        TableColumn<Contact,String> columnContactSuijet =new TableColumn("suijet");
        TableColumn<Contact,String> columnContactMail =new TableColumn("mail");
        TableColumn<Contact,String> columnContactDescrption =new TableColumn("descrption");
        TableColumn<Contact,String> columnContactEtate =new TableColumn("Etat");
        
        
        columnContactid.setCellValueFactory(new PropertyValueFactory<>("Id"));
        columnContactSuijet.setCellValueFactory(new PropertyValueFactory<>("suijet"));
        columnContactMail.setCellValueFactory(new PropertyValueFactory<>("mail"));
        columnContactDescrption.setCellValueFactory(new PropertyValueFactory<>("descrption"));
        columnContactEtate.setCellValueFactory(new PropertyValueFactory<>("Etat"));
        
        columnContactid.prefWidthProperty().bind(tab_contact.widthProperty().divide(5));
        columnContactSuijet.prefWidthProperty().bind(tab_contact.widthProperty().divide(5));
        columnContactMail.prefWidthProperty().bind(tab_contact.widthProperty().divide(5));
        columnContactDescrption.prefWidthProperty().bind(tab_contact.widthProperty().divide(5));
        columnContactEtate.prefWidthProperty().bind(tab_contact.widthProperty().divide(5));
        
        ////bindcolumn
        tab_contact.getColumns().addAll(columnContactid,columnContactSuijet,columnContactMail,columnContactDescrption,columnContactEtate);
        ContactService serv = new ContactService();
        data=serv.getAll();
        tab_contact.setItems(data);
        filteredData= new FilteredList<>(data, e -> true);
        // TODO
    }    

    @FXML
    private void showprofile(MouseEvent event) {
                         Contact contact  = tab_contact.getSelectionModel().getSelectedItem();
         Image img = new Image("file:"+contact.getImage());
         photo.setImage(img);
         Suijet.setText(contact.getSuijet());
         email.setText(contact.getMail());
         descrption.setText(contact.getDescrption()+"");
        Suijet.setVisible(true);
        email.setVisible(true);
         descrption.setVisible(true);

    }

    @FXML
    private void advancedSearch(KeyEvent event) {
                
                serachfield.textProperty().addListener((observableValue,oldValue,newValue) ->{
        filteredData.setPredicate((Predicate<? super Contact>) (Contact contact)->{
        if(newValue == null || newValue.isEmpty())
        {return true ;}
        String lowerCaseFilter =newValue.toLowerCase();
        
        if(contact.getSuijet().contains(newValue))
        {return true;}
        else if(contact.getMail().toLowerCase().contains(lowerCaseFilter))
        {return true;}
        else if(contact.getEtat().toLowerCase().contains(lowerCaseFilter))
        {return true;}
        return false ;
    });
    });
         SortedList<Contact> sortedData = new SortedList<>(filteredData);
         sortedData.comparatorProperty().bind(tab_contact.comparatorProperty());
         tab_contact.setItems(sortedData);
    }
    
}
