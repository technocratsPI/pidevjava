/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import entity.Contact;
import entity.user;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import service.ContactService;
import service.UserService;
import static views.CreateAccountController.color;
import static views.CreateAccountController.validateMail;
import static views.CreateAccountController.validateName;
import static views.CreateAccountController.validatePassword;

/**
 * FXML Controller class
 *
 * @author hajri
 */
public class ContactController implements Initializable {
    
    Alert alert = new Alert(Alert.AlertType.INFORMATION);

    @FXML
    private JFXTextField mailUser;
    @FXML
    private JFXButton imagebtn;
    @FXML
    private ImageView photoC;
    @FXML
    private JFXTextArea descrption;
    @FXML
    private JFXButton envoyer;
    @FXML
    private JFXButton home;
    @FXML
    private JFXComboBox<String> suijetC;
    
    File image;
    File selectedFile;
    @FXML
    private AnchorPane Anchorcontact;
    @FXML
    private Pane gmap_pane;
    GoogleMap gmap;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // TODO
        suijetC.getItems().add("Feedback");
        suijetC.getItems().add("bug");
        suijetC.getItems().add("CompteSupendu");
        suijetC.getItems().add("other");
        suijetC.setValue("feedBack");
        
        user u = UserSession.getInstace().getUser();
        
        mailUser.setText(u.getEmail());
        
                   gmap = new GoogleMap();
        
        gmap_pane.getChildren().add(gmap.getwebview());
        
    }    

    @FXML
    private void uploadimage(ActionEvent event) {
         FileChooser fc = new FileChooser();
         fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JPG files","*.jpg"),new FileChooser.ExtensionFilter("PNG files","*.png"),new FileChooser.ExtensionFilter("JPEG files","*.jpeg"));
        selectedFile =fc.showOpenDialog(null);
         Image img = new Image("file:"+selectedFile.getPath());
         photoC.setImage(img);
    }

    @FXML
    private void validate(ActionEvent event) {
                
      
              
             try {
                   Contact c =new Contact();
                 
      
      
      if(selectedFile==null)
                     {
                    c = new Contact(suijetC.getValue(),mailUser.getText(), descrption.getText(),"Nontraitee", null);
                   
                         }      
                    
          else  
                 {
                     image= new File("C:/wamp64/www/image/"+mailUser.getText()+selectedFile.getName());
                       ImageIO.write(SwingFXUtils.fromFXImage(new Image("file:"+selectedFile.getPath()), null),"PNG",new File("C:/wamp64/www/image/"+mailUser.getText()+selectedFile.getName()));
                        c = new Contact(suijetC.getValue(),mailUser.getText(), descrption.getText(),"Nontraitee", "C:/wamp64/www/image/"+mailUser.getText()+selectedFile.getName());
                
                  }
                  ContactService Sc= new ContactService();
                  Sc.insert(c);
                  
                 alert.setTitle("Inscription");
                 alert.setHeaderText("votre Demande a ete envoyer !");
                 alert.showAndWait();
                 Parent root = FXMLLoader.load(getClass().getResource("home.fxml"));
                 Stage app_stage =(Stage) ((Node) event.getSource()).getScene().getWindow();
                 Scene scene = new Scene(root);
                 app_stage.setScene(scene);
                 app_stage.show();
                 
            } catch (IOException ex) {
                 Logger.getLogger(CreateAccountController.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
    
        @FXML
    private void gotohome(ActionEvent event) {
        try {
            Anchorcontact.getScene().setRoot(FXMLLoader.load(getClass().getResource("home.fxml")));
                    } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        

    }
    
    }

    @FXML
    private void map_clicked(MouseEvent event) {
          gmap.activateListener();
        gmap.setdepMarkerPosition(gmap.currentlat, gmap.currentlng);
        System.out.println(gmap.currentlat);
    }
}

    

