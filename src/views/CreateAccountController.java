/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import entity.user;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import service.UserService;

/**
 * FXML Controller class
 *
 * @author hajri
 */
public class CreateAccountController implements Initializable {

    @FXML
    private TextField fNom;
    @FXML
    private TextField fPrenom;
    @FXML
    private TextField fUsername;
    @FXML
    private TextField fEmail;
    @FXML
    private PasswordField fPassword;
    
    private File selectedFile ;  
    private static boolean valid  ;
    
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    File image ;
    @FXML
    private Button imagebtn;
    @FXML
    private AnchorPane AnchorCreateAcc;
    @FXML
    private Label errorNom;
    @FXML
    private Label errorPrenom;
    @FXML
    private Label errormail;
    @FXML
    private Label errorUsername;
    @FXML
    private Label errorPassword;
    @FXML
    private PasswordField fPasswordC;
    @FXML
    private Label errorPasswordC;
    @FXML
    private ImageView fdisplay;
    @FXML
    private TextField fphoto;
    @FXML
    private Button back;
    
    
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fphoto.setText("");
                
        
valid =true ;    
    }    
    
     public static void color(TextField tf,String color) {
        ObservableList<String> styleClass = tf.getStyleClass();
        if (color.equals("red")) {
            if (! styleClass.contains("error")) {
                styleClass.add("error");
            }
        } else {
            // remove all occurrences:
            styleClass.remove("error");                   
        }
    }
      public static final Pattern CHARACTHERS_ONLY = 
    Pattern.compile("^[a-zA-Z ]+$", Pattern.CASE_INSENSITIVE);
      public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
      public static final Pattern VALID_PASSWORD_REGEX = 
    Pattern.compile("^[a-zA-Z0-9]{6,12}$", Pattern.CASE_INSENSITIVE);
      
      public static final Pattern VALID_NUMBER = 
    Pattern.compile("^[0-9]{8}", Pattern.CASE_INSENSITIVE);

    public static boolean validateMail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find(); }
    public static boolean validateName(String nameStr) {
        Matcher matcher = CHARACTHERS_ONLY .matcher(nameStr);
        return matcher.find(); }
    public static boolean validatePassword(String passStr) {
        Matcher matcher = VALID_PASSWORD_REGEX .matcher(passStr);
        return matcher.find(); }
     public static boolean validateNumber(String numberStr) {
        Matcher matcher = VALID_NUMBER .matcher(numberStr);
        return matcher.find(); }
     
         private String createName ()
    {
        UserService us = new UserService();
          
           int x = us.getUsers().size()+1;
            String nom = "Capture"+x+"";
           System.out.println("***************");
           System.out.println(nom);
           System.out.println("***************");

           return nom;
   }
     
    @FXML
    private void CreateUser(ActionEvent event)  { 
        
              user usertest =new user() ;
              
        usertest.setEmail(fEmail.getText());
         usertest.setUsername(fUsername.getText());
        UserService servtest  = new UserService();
         if(fNom.getText().isEmpty())
        { color(fNom,"red");errorNom.setText("Ce champ est obligatoire !");valid=false;}
         else if(!validateName(fNom.getText()))
        { color(fNom,"red");errorNom.setText("Le nom doit contenir que des caracthéres !");valid=false;}  
         
         if(fPrenom.getText().isEmpty())
        { color(fPrenom,"red");errorPrenom.setText("Ce champ est obligatoire !");valid=false;}
         else if(!validateName(fPrenom.getText()))
        { color(fPrenom,"red");errorPrenom.setText("Le prenom doit contenir que des caracthéres !");valid=false;}
         
         if(fEmail.getText().isEmpty())
        { color(fEmail,"red");errormail.setText("Ce champ est obligatoire !");valid=false;}
         else if(!validateMail(fEmail.getText()))
        { color(fEmail,"red");errormail.setText("Adresse mail non valide !");valid=false;}
         else if(servtest.searchByMail(usertest)!=null)
         {color(fEmail,"red");errormail.setText("Adresse mail deja utilisé !");valid=false;}
        
                   if(fUsername.getText().isEmpty())
        { color(fUsername,"red");errorUsername.setText("Ce champ est obligatoire !");valid=false;}
          else if(!validatePassword(fUsername.getText()))
        { color(fUsername,"red");errorUsername.setText("Username trop court ou trop long !");valid=false;}
          else if(servtest.searchByUsrname(usertest)!=null)
         {color(fUsername,"red");errorUsername.setText("Username deja utilisé !");valid=false;}   
                   
          if(fPassword.getText().isEmpty())
        { color(fPassword,"red");errorPassword.setText("Ce champ est obligatoire !");valid=false;}
          else if(!validatePassword(fPassword.getText()))
        { color(fPassword,"red");errorPassword.setText("Motdepasse trop court ou trop long !");valid=false;}
          
          
            if(fPasswordC.getText().isEmpty())
        { color(fPasswordC,"red");errorPasswordC.setText("Ce champ est obligatoire !");valid=false;}
             else if(!fPasswordC.getText().equals(fPassword.getText()))
        { color(fPasswordC,"red");errorPasswordC.setText("les deux mot de passe ne sont pas identiques!");valid=false;}
               
            
            
        if (valid == true)
        {
             try {
                 user user =new user() ;
                 
      
      
      if(selectedFile==null)
                     {
                         if(fphoto.getText().isEmpty())   
                         {
                   user = new user(fNom.getText(), fPrenom.getText(),null,fUsername.getText(),fEmail.getText(),fPasswordC.getText(),1,"actif");
                     }
                  
                         else{
                             
                    user = new user(fNom.getText(), fPrenom.getText(),"C:/wamp64/www/image/"+createName()+".png",fUsername.getText(),fEmail.getText(),fPasswordC.getText(),1,"actif");
                   
                         }      
                     }
          else  
                 {
                     image= new File("C:/wamp64/www/image/"+fUsername.getText()+selectedFile.getName());
                       ImageIO.write(SwingFXUtils.fromFXImage(new Image("file:"+selectedFile.getPath()), null),"PNG",new File("C:/wamp64/www/image/"+fUsername.getText()+selectedFile.getName()));
                       user = new user(fNom.getText(), fPrenom.getText(),"C:/wamp64/www/image/"+fUsername.getText()+selectedFile.getName(),fUsername.getText(),fEmail.getText(),fPasswordC.getText(),1,"actif");

                
                  }
                  UserService serv =new UserService();
                  serv.insert(user);
                 alert.setTitle("Inscription");
                 alert.setHeaderText("Compte créé avec Succès !");
                 alert.setContentText("Vous pouvez connecter dès maintenant .");
                 alert.showAndWait();
                 Parent root = FXMLLoader.load(getClass().getResource("ss.fxml"));
                 Stage app_stage =(Stage) ((Node) event.getSource()).getScene().getWindow();
                 Scene scene = new Scene(root);
                 app_stage.setScene(scene);
                 app_stage.show();
                 
            } catch (IOException ex) {
                 Logger.getLogger(CreateAccountController.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
        valid=true ;
        
}
    


    @FXML
    private void uploadImage(ActionEvent event) {
         FileChooser fc = new FileChooser();
         fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JPG files","*.jpg"),new FileChooser.ExtensionFilter("PNG files","*.png"),new FileChooser.ExtensionFilter("JPEG files","*.jpeg"));
        selectedFile =fc.showOpenDialog(null);
         Image img = new Image("file:"+selectedFile.getPath());
         fdisplay.setImage(img);
        
    }

    @FXML
    private void handleMsgKey(KeyEvent event) {
        color(fNom, "blue");
        color(fPrenom, "blue");
         color(fEmail, "blue");
          color(fUsername, "blue");
           color(fPassword, "blue");
           color(fPasswordC, "blue");
            
         
            errorNom.setText("");
             errorPrenom.setText("");
             errormail.setText("");
             errorUsername.setText("");
             errorPassword.setText("");
             errorPasswordC.setText("");
    }

    @FXML
    private void Capture(ActionEvent event) throws IOException {
      ToolBarAssistant.loadWindow(getClass().getResource("captureWindow.fxml"), "Prendre Une Photo", null);
      
      
 }

    @FXML
    private void gotologin(ActionEvent event) throws IOException {
             AnchorPane pane = FXMLLoader.load(getClass().getResource("ss.fxml"));
      AnchorCreateAcc.getChildren().setAll(pane);
        
    }

    @FXML
    private void setphoto(MouseEvent event) {
        fphoto.setText("capture");
    }


    
}

