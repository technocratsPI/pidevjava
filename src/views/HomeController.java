/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author hajri
 */
public class HomeController implements Initializable {

    @FXML
    private AnchorPane AnchorHome;
    @FXML
    private Tab Contactbtn;
    @FXML
    private Tab ServiceBtn;
    @FXML
    private Tab Reclamationbtn;
    @FXML
    private Tab deconnxion;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void gotoContact(Event event) {
                try {
            AnchorHome.getScene().setRoot(FXMLLoader.load(getClass().getResource("Contact.fxml")));
                    } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void gotoservice(Event event) {
                try {
            AnchorHome.getScene().setRoot(FXMLLoader.load(getClass().getResource("Service.fxml")));
                    } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void gotoreclamation(Event event) {
        
    }

    @FXML
    private void gotoss(Event event) {
                
        try {
            AnchorHome.getScene().setRoot(FXMLLoader.load(getClass().getResource("ss.fxml")));
                    } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
