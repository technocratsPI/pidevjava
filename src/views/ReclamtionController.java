  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import entity.Reclamation;
import entity.user;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import service.ContactService;
import service.ReclamationService;

/**
 * FXML Controller class
 *
 * @author hajri
 */
public class ReclamtionController implements Initializable {
user user = new user();
    Alert alert = new Alert(Alert.AlertType.INFORMATION);

    @FXML
    private JFXButton create;
    @FXML
    private JFXComboBox<String> suijet;
    @FXML
    private JFXTextArea descr;

    public user getUser() {
        return user;
    }

    public void setUser(user user) {
        this.user = user;
    }
    @FXML
    private AnchorPane reclamationpane;
    @FXML
    private JFXButton home;
user u = UserSession.getInstace().getUser();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         // TODO
        suijet.getItems().add("spammer offencef language");
        suijet.getItems().add(".......");
        suijet.getItems().add("int1");
        suijet.setValue("........");
        
        
        // TODO
    }    

    @FXML
    private void gotohome(ActionEvent event) throws IOException {
                               AnchorPane pane = FXMLLoader.load(getClass().getResource("admin.fxml"));
           reclamationpane.getChildren().setAll(pane);
    }

    @FXML
    private void createreclamation(ActionEvent event) throws IOException {
        try {
          
            Reclamation r = new Reclamation(suijet.getValue(),u.getEmail(), descr.getText(), u.getId(), user.getId());
                              ReclamationService Sc= new ReclamationService();
                  Sc.insert(r);
                  
                 alert.setTitle("Reclamation");
                 alert.setHeaderText("votre Demande a ete envoyer !");
                 alert.showAndWait();
                 Parent root = FXMLLoader.load(getClass().getResource("admin.fxml"));
                 Stage app_stage =(Stage) ((Node) event.getSource()).getScene().getWindow();
                 Scene scene = new Scene(root);
                 app_stage.setScene(scene);
                 app_stage.show();
        }
        catch(IOException ex) {
                 Logger.getLogger(ReclamtionController.class.getName()).log(Level.SEVERE, null, ex);
             }
    }
}