/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import entity.Service;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import service.ServiceService;

/**
 * FXML Controller class
 *
 * @author hajri
 */
public class ServiceController implements Initializable {
    

    @FXML
    private AnchorPane rootPane;
    @FXML
    private TableView<Service> tab_Service;
    @FXML
    private TableColumn<Service,Integer> service_id;
    @FXML
    private TableColumn<Service,String> Service_nom;

    ObservableList<Service> data =FXCollections.observableArrayList();
    @FXML
    private Button back;
    @FXML
    private ButtonBar menuBar;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

     service_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        Service_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        
                service_id.prefWidthProperty().bind(tab_Service.widthProperty().divide(5));
        Service_nom.prefWidthProperty().bind(tab_Service.widthProperty().divide(5));
        
        tab_Service.getColumns().addAll(service_id,Service_nom);
        
        ServiceService ser = new ServiceService();
        data = ser.getAll();
        tab_Service.setItems(data);

    }

    @FXML
    private void gotoss(ActionEvent event) {
                        try {
            rootPane.getScene().setRoot(FXMLLoader.load(getClass().getResource("ss.fxml")));
                    } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void showbutton(MouseEvent event) {
        menuBar.setVisible(true);
    }
    
    
}
