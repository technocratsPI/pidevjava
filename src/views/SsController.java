/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import entity.user;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import service.UserService;

/**
 * FXML Controller class
 *
 * @author hajri
 */
public class SsController implements Initializable {

    private AnchorPane AnchorPane1;
    @FXML
    private AnchorPane AnchorLogin;
    @FXML
    private TextField loginField;
    @FXML
    private TextField passField;
    @FXML
    private Label errorLogin;
    @FXML
    private Label errorPass;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

          public static void color(TextField tf,String color) {
        ObservableList<String> styleClass = tf.getStyleClass();
        if (color.equals("red")) {
            if (! styleClass.contains("error")) {
                styleClass.add("error");
            }
        } else {
            // remove all occurrences:
            styleClass.remove("error");                   
        }
    }
        
    @FXML
    private void GoToCreerAccount(ActionEvent event) {
        
ToolBarAssistant.loadWindow(getClass().getResource("CreateAccount.fxml"), "FixITCrateAccount", null);
         Stage s =(Stage) AnchorLogin.getScene().getWindow();
        s.close();
    }

    @FXML
    private void handleLogin(ActionEvent event) {
                user user = new user();
        UserService serv =new UserService();
        user.setUsername(loginField.getText());
        user =serv.searchByUsrname(user);
        if(loginField.getText().isEmpty())
        {
         //empty login field
            
            color(loginField, "red");
            errorLogin.setText("champ vide !");
        }
        else if(passField.getText().isEmpty())
        {
        // empty pass field
             color(passField, "red");
            
            errorPass.setText("champ vide !");
        }
        else if(user==null)
        {
        //  mail not found
            color(loginField, "red");
            
            errorLogin.setText("utilisateur non exsistant !");
            
        }
        else if (!user.getPassword().equals(passField.getText()))
        {
        // wrong password
            color(passField, "red");
            
            errorPass.setText("Mot de passe incorrect !");
        }
        else if (user.getStatut().equals("susspendu"))
        {
        // account suspended
             System.out.println("account susspended !");
        }
        
        else if (user.getRole()==1)
                {

            UserSession session = UserSession.getInstace(user);
            ToolBarAssistant.loadWindow(getClass().getResource("home.fxml"), "FixIt", null);
         Stage s =(Stage) AnchorLogin.getScene().getWindow();
        s.close();
        
        }
        else
        {
        
             UserSession session = UserSession.getInstace(user);
             ToolBarAssistant.loadWindow(getClass().getResource("admin.fxml"), "FixITBack", null);
         Stage s =(Stage) AnchorLogin.getScene().getWindow();
        s.close();
          
        
        
            
            
        
        }
    }

    @FXML
    private void handleMsgError(KeyEvent event) {
        
        color(loginField, "blue");
        color(passField, "blue");
            
        errorLogin.setText("");
        errorPass.setText("");
    }

    @FXML
    private void handleMsgError(MouseEvent event) {
              color(loginField, "blue");
        color(passField, "blue");
            
        errorLogin.setText("");
        errorPass.setText("");
    }

    @FXML
    private void handleMsgError(InputMethodEvent event) {
        color(loginField, "blue");
        color(passField, "blue");
            
        errorLogin.setText("");
        errorPass.setText("");
    }
    
}

