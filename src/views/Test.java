/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import entity.user;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import service.UserService;

/**
 *
 * @author hajri
 */
public class Test extends Application {
    
       @Override
    public void start(Stage stage) throws Exception { 
       Parent root = (AnchorPane) FXMLLoader.load(getClass().getResource("ss.fxml"));
       
        Scene scene = new Scene(root);
        stage.setScene(scene);
        
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Date d = Date.from(Instant.MIN);
        user User = new user();
        UserService serv =new UserService();
        launch();
    }
    
}
