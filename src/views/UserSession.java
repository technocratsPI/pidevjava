/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import entity.user;

public  class UserSession {

    private static UserSession instance;



    private user user =new user();
    

    private UserSession(user user) {
        this.user=user;
    
    }

    public static UserSession getInstace(user user) {
      
        instance = new UserSession(user);
        
        return instance;
    }
    public static UserSession getInstace() {
        
        return instance;
    }

    public user getUser() {
        return user;
    }

  

    public void cleanUserSession() {
        user=null;
    }

    @Override
    public String toString() {
        return "User" + user +"}";
    }
}